<?php

namespace App\Http\Controllers;
use App\Product;
use App\Provider;
use App\Category;
use Illuminate\Http\Request;
use App\Http\Request\Product\StoreRequest;
use App\Http\Request\Product\UpdateRequest;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Barryvdh\DomPDF\Facade\Pdf; 

class ProductController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('can:products.create')->only(['create','store']);
        $this->middleware('can:products.index')->only(['index']);
        $this->middleware('can:products.edit')->only(['edit','update']);
        $this->middleware('can:products.show')->only(['show']);
        $this->middleware('can:products.destroy')->only(['destroy']);
        $this->middleware('can:change.status.products')->only(['change_status']);
        $this->middleware('can:products.pdf')->only(['pdf']);
        

    }

    public function index()
    {
         $products = Product::orderBy('provider_id','asc')->orderBy('category_id','asc')->orderBy('name','asc')->paginate(100);
        return view('admin.product.index',compact('products'));
    }

  
    public function create()
    {
        $categories = Category::orderBy('name','asc')->get();
        $providers = Provider::get();
        return view('admin.product.create',compact('categories','providers'));
    }


    public function store(StoreRequest $request)
    {
        if ($request -> hasFile('picture')) {
            $file = $request -> file('picture');
            $image_name = time().'_'.$file->getClientOriginalName();
            $file->move(public_path("/image"),$image_name);
            
            $product = Product::create($request->all()+[
                'image'=>$image_name,
            ]);
        }else{
            $product = Product::create($request->all());
        }


        $product->update(['code'=>$product->id]);
        return redirect()->route('products.index');
    }

  
    public function show(Product $product)
    {
        return view('admin.product.show',compact('product'));
    }


    public function edit(Product $product)
    {
        $categories = Category::get();
        $providers = Provider::get();
        return view('admin.product.edit',compact('product','categories','providers'));
    }

  
    public function update(UpdateRequest $request, Product $product)
    {
        if ($request -> hasFile('picture')) {
            $file = $request -> file('picture');
            $image_name = time().'_'.$file->getClientOriginalName();
            $file->move(public_path("/image"),$image_name);
         
            $product->update($request->all()+[
                'image'=>$image_name,
            ]);
        }
        $product->update($request->all());

        return redirect()->route('products.index');
    }

  
    public function destroy(Product $product)
    {
        $product->delete();
        return redirect()->route('products.index');
    }    

    public function change_status(Product $product)
    {
        if ($product->status == 'ACTIVO') {
            $product->update(['status'=>'INACTIVO']);
            return redirect()->back();
        } else {
            $product->update(['status'=>'ACTIVO']);
            return redirect()->back();
        } 
    }

    public function search(Request $request){
        $term = $request->get('term');

        $querys =  Product::where('name','LIKE','%'. $term . '%')->get();

        $data = [];
        $nombre;
        $stock;
        
        foreach($querys as $query){
            $data[] = [
                'label' => $query->code.", ".$query->category->name. ",  " .$query->name. ",  Existencias = " .$query->stock. ",  Precio = ".$query->price
    
            ];
        }
        return $data;
    }

    public function pdf()
    {  
        $products = Product::orderBy('provider_id','asc')->orderBy('category_id','asc')->orderBy('name','asc')->get();
        $date = Carbon::now('america/Guatemala');
        $pdf = PDF::loadView('admin.product.pdf', compact('products','date'));
        return $pdf->download('Reporte_de_inventario_.pdf');

    }
}