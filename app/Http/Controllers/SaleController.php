<?php

namespace App\Http\Controllers;

use App\Sale;
use App\Client;
use App\User;
use App\Product;
use App\Category;
use Illuminate\Http\Request;
use App\Http\Request\Sale\StoreRequest;
use App\Http\Request\Sale\UpdateRequest;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use Barryvdh\DomPDF\Facade\Pdf;
class SaleController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('can:sales.create')->only(['create','store']);
        $this->middleware('can:sales.index')->only(['index']);
        $this->middleware('can:sales.show')->only(['show']);
        
        $this->middleware('can:change.status.sales')->only(['change_status']);
        $this->middleware('can:sales.pdf')->only(['pdf']);
 

    }

    public function index(){
    $sales = Sale::paginate(5);
    return view('admin.sale.index', compact('sales'));
}
public function create()
{
    //$clients  = Client::role('Client')->get();
    //$products = Product::pos_products()->get();
    $clients = Client::get();
	$categories = Category::get();
    // $products = Product::get();
    $products = Product::orderBy('provider_id','asc')->orderBy('name','asc')->where('status','ACTIVO')->get();
    return view('admin.sale.create', compact('clients', 'products','categories'));
}
public function store(StoreRequest $request, Sale $sale)
{
    //$sale->my_store($request);
    $sale = Sale::create($request->all()+[
        'user_id'=>Auth::user()->id,
        'sale_date'=>Carbon::now('America/guatemala'),
    ]);
    foreach($request->product_id as $key=>$product){
        $results[] = array("product_id"=>$request->product_id[$key],
        "quantity" => $request->quantity[$key], "price"=>$request->price[$key],
        "discount"  => $request->discount[$key]);
    }
    $sale -> saleDetails()->createMany($results);
    return redirect()->route('sales.index')->with('toast_success', '¡Venta registrada con éxito!');
}
public function show(Sale $sale)
{
    $subtotal = 0 ;
    $saleDetails = $sale->saleDetails;
    foreach ($saleDetails as $saleDetail) {
        $subtotal += $saleDetail->quantity*$saleDetail->price
        -($saleDetail->quantity* $saleDetail->price)*$saleDetail->discount/100;
    }
    return view('admin.sale.show', compact('sale', 'saleDetails', 'subtotal'));
}
public function edit(Sale $sale)
{
    // $clients = Client::get();
    // return view('admin.sale.edit', compact('sale'));
}
public function update(UpdateRequest $request, Sale $sale)
{
    // $sale->update($request->all());
    // return redirect()->route('sales.index');
}
public function destroy(Sale $sale)
{
    // $sale->delete();
    // return redirect()->route('sales.index');
}
public function pdf(Sale $sale)
{
    $subtotal = 0 ;
    $ahorro = 0 ;

    $saleDetails = $sale->saleDetails;
    foreach ($saleDetails as $saleDetail) {
        $subtotal += $saleDetail->quantity*$saleDetail->price-$saleDetail->quantity* $saleDetail->price*$saleDetail->discount/100;
        $ahorro += $saleDetail->quantity* $saleDetail->price*$saleDetail->discount/100; 
       
    }
    $pdf = PDF::loadView('admin.sale.pdf', compact('sale', 'subtotal','ahorro', 'saleDetails'));
    return $pdf->download('Reporte_de_venta_'.$sale->id.'.pdf');
}

public function print(Sale $sale){
    try {
        $subtotal = 0 ;
        $saleDetails = $sale->saleDetails;
        foreach ($saleDetails as $saleDetail) {
            $subtotal += $saleDetail->quantity*$saleDetail->price-$saleDetail->quantity* $saleDetail->price*$saleDetail->discount/100;
        }  

        $printer_name = "TM20";
        $connector = new WindowsPrintConnector($printer_name);
        $printer = new Printer($connector);

        $printer->text("€ 9,95\n");

        $printer->cut();
        $printer->close();


        return redirect()->back();

    } catch (\Throwable $th) {
        return redirect()->back();
    }
}

public function change_status(Sale $sale)
{
    if ($sale->status == 'VALID') { 
        $sale->update(['status'=>'CANCELED']);
        return redirect()->back();
    } else {
        $sale->update(['status'=>'VALID']);
        return redirect()->back();
    } 
}
   
}
