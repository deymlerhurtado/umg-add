<?php

namespace App\Http\Request\Category;

use Illuminate\Foundation\Http\FormRequest;

class StoreRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'name'=>'required|string|max:50',
            'description'=>'nullable|string|max:255',
        ];
    }

    public function messages()
    {
        return[
            'name.required'=>'Este campo es requerido',
            'name.string'=>'el valor no es correcto',
            'name.max'=>'solo se permiten 50 caracteres',
            
            'description.string'=>'el valor no es correcto',
            'description.max'=>'solo se permiten 255 caracteres',
        ];
    }
}
