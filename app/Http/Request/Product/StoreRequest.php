<?php

namespace App\Http\Request\Product;

use Illuminate\Foundation\Http\FormRequest;

class StoreRequest extends FormRequest
{
   
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            
            'name'=>'required|string|max:255',
            // 'lote'=>'required|integer|',
            // 'f_fabricacion'=>'nullable|date|',
            // 'f_vencimento'=>'nullable|date|',
            //'image'=>'nullable|dimensions:min_width=100,min_height=200',
            'price'=>'required|',   
            'category_id'=>'required|integer|exists:App\Category,id',
            'provider_id'=>'required|integer|exists:App\Provider,id',
        
        ];
    }

    public function messages()
    {
        return[
            'name.required'=>'el campo es requerido',
            'name.string'=>'el valor no es correcto',
            'name.max'=>'solo se permite 255 caracteres',

            'lote.required'=>'el campo es requerido',
            'lote.integer'=>'eel valor no es correcto',
          
            'f_fabricacion.date'=>'El valor no es correcto',
            'f-vencimiento.date'=>'El valor no es correcto',

            'image.dimensions'=>'Solo se permien imagenes de 100x200px',
            
            'price.required'=>'valor requerido',

            'category_id.integer'=>'el valor tiene que ser entero',
            'category_id.required'=>'el campo es requerido',
            'category_id.exists'=>'la categoria no existe',

            'provider_id.integer'=>'el valor tiene que ser entero',
            'provider_id.required'=>'el campo es requerido',
            'provider_id.exists'=>'el proveedor  no existe',
        ];

    }
}
