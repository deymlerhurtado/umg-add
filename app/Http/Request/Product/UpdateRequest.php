<?php

namespace App\Http\Request\Product;

use Illuminate\Foundation\Http\FormRequest;

class UpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            
            'name'=>'required|string|unique:products,name,
            '.$this->route('product')->id.'|max:255',
           'image'=>'nullable',
            'price'=>'required|',   

 
        
        ];
    }
 
    public function messages()
    {
        return[
            'name.required'=>'el campo es requerido',
            'name.string'=>'el valor no es correcto',
            'name.max'=>'solo se permite 255 caracteres',
            'empresa.unique'=>'el producto ya se encuentra registrado',

            'lote.required'=>'el campo es requerido',
            'lote.integer'=>'eel valor no es correcto',
          
            'f_fabricacion.date'=>'El valor no es correcto',
            'f-vencimiento.date'=>'El valor no es correcto',

            'image.dimensions'=>'Solo se permien imagenes de 100x200px',
            
            'price.required'=>'valor requerido',

            'category_id.integer'=>'el valor tiene que ser entero',
            'category_id.required'=>'el campo es requerido',
            'category_id.exists'=>'la categoria no existe',

            'provider_id.integer'=>'el valor tiene que ser entero',
            'provider_id.required'=>'el campo es requerido',
            'provider_id.exists'=>'el proveedor  no existe',
        ];

    }
}
