<?php

namespace App\Http\Request\Provider;

use Illuminate\Foundation\Http\FormRequest;

class UpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'=>'required|string|max:255',
            'numero'=>'required|string',
             'empresa'=>'required|string|max:255',
             'email'=>'nullable|email|string',

        ];
    }

    public function messages()
    {
        return[
            'name.required'=>'Este campo es requerido',
            'name.string'=>'el valor no es correcto',
            'name.max'=>'solo se permiten 255 caracteres',
            
            'numero.required'=>'Este campo es requerido',
            'numero.string'=>'el valor no es correcto',
            'numero.max'=>'numero telefonico de 8 a 50 caracteres',
            'numero.unique'=>'ya se encuentra registrado',

            'empresa.required'=>'Este campo es requerido',
            'empresa.string'=>'el valor no es correcto',
            'empresa.max'=>'solo se aceptan 255 caracteres',
            'empresa.unique'=>'ya se encuentra registrado',
            
            'email.email'=>'no es un correo electronico',
            'email.string'=>'el valor no es correcto',
            'email.max'=>'solo se aceptan 255 caracteres',
            'email.unique'=>'ya se encuentra registrado',
        ];
    }
}
