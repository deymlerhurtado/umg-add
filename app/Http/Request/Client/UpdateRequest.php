<?php

namespace App\Http\Request\Client;

use Illuminate\Foundation\Http\FormRequest;

class UpdateRequest extends FormRequest
{

    public function authorize()
    {
        return true;
    }

   
    public function rules()
    {
        return [
            'name'=>'string|required|max:255',
            'apellido'=>'string|required|max:255',
            'dpi'=>'string|required|unique:clients,dpi,'.$this->route('client')->id.'|min:13|max:13',
            'direccion'=>'string|required|max:255',
            'phone'=>'integer|nullable|unique:clients,phone,'.$this->route('client')
            ->id.'',
            'email'=>'string|nullable|unique:clients,email,'.$this->route('client')
            ->id.'|max:255',
        ];
    }

    public function messages()
    {
        return[
            'name.required'=>'el campo es requerido',
            'name.string'=>'el valor no es correcto',
            'name.max'=>'solo se permite 255 caracteres',
            'apellido.required'=>'el campo es requerido',
            'apellido.string'=>'el valor no es correcto',
            'apellido.max'=>'solo se permite 255 caracteres',

            'dpi.string'=>'el valor no es correcto',
            'dpi.required'=>'el campo es requerido',
            'dpi.unique'=>'este DPI ya se encuentra registrado',
            'dpi.min'=>'ser requiere de 13 caracteres',
            'dpi.max'=>'solo se permiten 8 caracteres',
            'direccion.required'=>'el campo es requerido',
            'direccion.string'=>'el valor no es correcto',
            'direccion.max'=>'solo se permite 255 caracteres',

            'phone.string'=>'el valor es incorrecto',
            'phone.unique'=>'el numero celular ya esta registrado',
            'phone.min'=>'se requiere de 8 caracteres',
            'phone.max'=>'solo se permiten 8 caracteres',

            'email.string'=>'el valor es incorrecto',
            'email.unique'=>'direccion de correo electronico ya registrada',
            'email.max'=>'solo se permiten 255 caracteres',
            'email.email'=>'no es un correo electronico valido',          
        ];
    }
}
