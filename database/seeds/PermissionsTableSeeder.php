<?php

use Illuminate\Database\Seeder;
use Caffeinated\Shinobi\Models\Permission;
class PermissionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    { 
    // usuarios
        Permission::create([
            'name'=>'navegar usuarios',
            'slug'=>'users.index',
            'description'=>'lista y navega usuarios',
        ]);
        Permission::create([
            'name'=>'Creacion usuarios',
            'slug'=>'users.create',
            'description'=>'crea usuarios',
        ]);
        Permission::create([
            'name'=>'detalles usuarios',
            'slug'=>'users.show',
            'description'=>'ver detalles usuarios',
        ]);
        Permission::create([
            'name'=>'edicion usuarios',
            'slug'=>'users.edit',
            'description'=>'editar usuarios',
        ]);
        Permission::create([
            'name'=>'Eliminar usuarios',
            'slug'=>'users.destroy',
            'description'=>'eliminar usuarios',
        ]);
          // roles
          Permission::create([
            'name'=>'navegar roles',
            'slug'=>'roles.index',
            'description'=>'lista y navega usuarios',
        ]);
        Permission::create([
            'name'=>'Creacion roles',
            'slug'=>'roles.create',
            'description'=>'crea roles',
        ]);
        Permission::create([
            'name'=>'detalles roles',
            'slug'=>'roles.show',
            'description'=>'ver detalles roles',
        ]);
        Permission::create([
            'name'=>'edicion roles',
            'slug'=>'roles.edit',
            'description'=>'editar roles',
        ]);
        Permission::create([
            'name'=>'Eliminar roles',
            'slug'=>'roles.destroy',
            'description'=>'eliminar roles',
        ]);
        //categorias
        Permission::create([
            'name'=>'navegar categorias',
            'slug'=>'categories.index',
            'description'=>'lista y navega categorias',
        ]);
        Permission::create([
            'name'=>'detalles categorias',
            'slug'=>'categories.show',
            'description'=>'ver detalles categorias',
        ]);
        Permission::create([
            'name'=>'edicion categorias',
            'slug'=>'categories.edit',
            'description'=>'editar categorias',
        ]);
        Permission::create([
            'name'=>'Creacion categorias',
            'slug'=>'categories.create',
            'description'=>'crea categoria',
        ]);
        Permission::create([
            'name'=>'Eliminar categorias',
            'slug'=>'categories.destroy',
            'description'=>'eliminar categorias',
        ]);
        //clientes
        Permission::create([
            'name'=>'navegar clientes',
            'slug'=>'clients.index',
            'description'=>'lista y navega clientes',
        ]);
        Permission::create([
            'name'=>'detalles clientes',
            'slug'=>'clients.show',
            'description'=>'ver detalles clientes',
        ]);
        Permission::create([
            'name'=>'edicion clientes',
            'slug'=>'clients.edit',
            'description'=>'editar clientes',
        ]);
        Permission::create([
            'name'=>'Creacion clientes',
            'slug'=>'clients.create',
            'description'=>'crea clientes',
        ]);
        Permission::create([
            'name'=>'Eliminar clientes',
            'slug'=>'clients.destroy',
            'description'=>'eliminar clientes',
        ]);
        //products
        Permission::create([
            'name'=>'navegar products',
            'slug'=>'products.index',
            'description'=>'lista y navega products',
        ]);
        Permission::create([
            'name'=>'detalles products',
            'slug'=>'products.show',
            'description'=>'ver detalles products',
        ]);
        Permission::create([
            'name'=>'edicion products',
            'slug'=>'products.edit',
            'description'=>'editar products',
        ]);
        Permission::create([
            'name'=>'Creacion products',
            'slug'=>'products.create',
            'description'=>'crea products',
        ]);
        Permission::create([
            'name'=>'Eliminar products',
            'slug'=>'products.destroy',
            'description'=>'eliminar products',
        ]);
        //providers
        Permission::create([
            'name'=>'navegar providers',
            'slug'=>'providers.index',
            'description'=>'lista y navega providers',
        ]);
        Permission::create([
            'name'=>'detalles providers',
            'slug'=>'providers.show',
            'description'=>'ver detalles providers',
        ]);
        Permission::create([
            'name'=>'edicion providers',
            'slug'=>'providers.edit',
            'description'=>'editar providers',
        ]);
        Permission::create([
            'name'=>'Creacion providers',
            'slug'=>'providers.create',
            'description'=>'crea providers',
        ]);
        Permission::create([
            'name'=>'Eliminar providers',
            'slug'=>'providers.destroy',
            'description'=>'eliminar providers',
        ]);
          //compras
          Permission::create([
            'name'=>'navegar compras',
            'slug'=>'purchases.index',
            'description'=>'lista y navega compras',
        ]);
        Permission::create([
            'name'=>'detalles compras',
            'slug'=>'purchases.show',
            'description'=>'ver detalles compras',
        ]);
        Permission::create([
            'name'=>'edicion providers',
            'slug'=>'purchases.edit',
            'description'=>'editar providers',
        ]);
        Permission::create([
            'name'=>'Creacion compras',
            'slug'=>'purchases.create',
            'description'=>'crea compras',
        ]);
        Permission::create([
            'name'=>'Eliminar compras',
            'slug'=>'purchases.destroy',
            'description'=>'eliminar compras',
        ]);
             //ventas
             Permission::create([
                'name'=>'navegar ventas',
                'slug'=>'sales.index',
                'description'=>'lista y navega ventas',
            ]);
            Permission::create([
                'name'=>'detalles ventas',
                'slug'=>'sales.show',
                'description'=>'ver detalles ventas',
            ]);
            Permission::create([
                'name'=>'edicion ventas',
                'slug'=>'sales.edit',
                'description'=>'editar ventas',
            ]);
            Permission::create([
                'name'=>'Creacion ventas',
                'slug'=>'sales.create',
                'description'=>'crea ventas',
            ]);
            Permission::create([
                'name'=>'Eliminar ventas',
                'slug'=>'sales.destroy',
                'description'=>'eliminar ventas',
            ]);
            //reportes
            Permission::create([
                'name'=>'Creacion reporte compra',
                'slug'=>'purchases.pdf',
                'description'=>'crea reportes pdf',
            ]);
            Permission::create([
                'name'=>'Creacion repo venta',
                'slug'=>'sales.pdf',
                'description'=>'reporte pdf',
            ]);
            //upload_purchase
            Permission::create([
                'name'=>'upload_purchase',
                'slug'=>'upload.purchases',
                'description'=>'carga de imagen',
            ]);
               //change_status
               Permission::create([
                'name'=>'cambia estado productos',
                'slug'=>'change.status.products',
                'description'=>'cambio estado',
            ]);
            Permission::create([
                'name'=>'cambia estado compra',
                'slug'=>'change.status.purchases',
                'description'=>'cambio estado',
            ]);
            Permission::create([
                'name'=>'cambia estado venta',
                'slug'=>'change.status.sales',
                'description'=>'cambio estado',
            ]);
            //reportes
            Permission::create([
             'name'=>'Reporte por dia',
            'slug'=>'reports.day',
            'description'=>'permite ver reporte del dia',
            ]);
            Permission::create([
            'name'=>'Reporte por fechas',
           'slug'=>'reports.date',
             'description'=>'permite ver reporte por rango de fechas',
              ]); 
             Permission::create([
             'name'=>'Reporte por resultado',
             'slug'=>'report.results',
             'description'=>'permite ver reporte',
            ]);
            

    

    }
}
