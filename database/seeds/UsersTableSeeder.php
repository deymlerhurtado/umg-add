<?php

use Illuminate\Database\Seeder;
use Caffeinated\Shinobi\Models\Role;
use App\User;
class UsersTableSeeder extends Seeder
{

    public function run()
    {
        Role::create([
            'name'=>'Admin',
            'slug'=>'admin',
            'special'=>'all-access',
        ]);

        $user = User::create([
            'name'=>'Yuren',
            'email'=>'yuren@gmail.com',
            'password'=>'$2y$10$.dR.DakVUiBPx9hK01O4NOzwj3px5dHoX.9ZwESXMBf1/EMsCL5eK',
        ]);
        $user->roles()->sync(1);
    }
}
