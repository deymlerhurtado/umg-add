@extends('layouts.admin')

@section('title','Editar Cliente')
@section('styles')

{!! Html::style('treegrid/css/jquery.treegrid.css') !!}
@endsection
@section('options')
@endsection
@section('preference')
@endsection
@section('content')
<div class="content-wrapper">
    <div class="page-header">
        <h3 class="page-title">
            Editar Clientes
        </h3>
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb breadcrumb-custom">

                <li class="breadcrumb-item"><a href="">Panel administrador</a></li>
                <li class="breadcrumb-item"><a href="{{route('clients.index')}}">Clientes</a></li>
                <li class="breadcrumb-item active" aria-current="page">Edicion de Cliente</li>
            </ol>
        </nav>
    </div>
    <div class="row">
        <div class="col-lg-12 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">
                    <div class="d-flex justify-content-between">
                        <h4 class="card-title">Registro de Clientes</h4>          
                    </div>
                    {!! Form::open(['route'=>['clients.update',$client],'method'=>'PUT','files' => true]) !!}
                    
                    <div class="form-group">
                        <label for="names">Nombre Cliente</label>
                        <input type="text"  class="form-control" name="name" id="name"  value="{{$client->name}}"
                                placeholder="Nombre" aria-describedby="helpId" required>
                    </div>
                    <div class="form-group">
                        <label for="apellidos">Apellido Cliente</label>
                        <input type="text"  class="form-control" name="apellido" id="apellido"  value="{{$client->apellido}}"
                                placeholder="Apellidos" aria-describedby="helpId" required>
                    </div>
                    <div class="form-group">
                        <label for="dpis">DPI (Documento Personal de Identificación)</label>
                        <input type="text" class="form-control" name="dpi" id="dpi"  value="{{$client->dpi}}"
                                placeholder="dpi" aria-describedby="helpId" required>
                    </div>
                    <div class="form-group">
                        <label for="direccions">Direccion de cliente</label>
                        <input type="text"  class="form-control" name="direccion" id="direccion" value="{{$client->direccion}}" 
                                placeholder="direccion" aria-describedby="helpId" required>
                    </div>
                    <div class="form-group">
                        <label for="phones">Numero de telefono</label>
                        <input type="number"  class="form-control" name="phone" id="phone" value="{{$client->phone}}"
                                placeholder="telefono" aria-describedby="helpId" required>
                    </div>
                    <div class="form-group">
                        <label for="emails">Correo electronico (opcional)</label>
                        <input type="email"  class="form-control" name="email" id="email" value="{{$client->email}}"
                                placeholder="email" aria-describedby="helpId">
                    </div>

                    <button type="submit" class="btn btn-primary mr-2">Actualizar</button>
                    <a href="{{route('clients.index')}}" class="btn btn-light">Cancelar</a>
                    {!! Form::close() !!}
                </div>
               {{--
                <div class="card-footer text-muted">
                    {{$clients->render()}}
                </div>
                --}}
            </div>
        </div>
    </div>
</div>
@endsection
@section('scripts')
{{!! Html::script('melody/js/dropify.js') !!}}
{!! Html::script('treegrid/js/jquery.treegrid.js') !!}
{!! Html::script('js/my_functions.js') !!}
<script type="text/javascript">
    $(document).ready(function() {
        $('.tree').treegrid().treegrid('collapseAll');
    });
</script>
@endsection
