@extends('layouts.admin')

@section('title','Gestión de Clientes')
@section('styles')
<style type="text/css">
    .unstyled-button{
        border: none;
        padding: 0;
        background: none; 
    }
</style>
{!! Html::style('treegrid/css/jquery.treegrid.css') !!}
@endsection
@section('options')
@endsection
@section('preference')
@endsection
@section('content')
<div class="content-wrapper">
    <div class="page-header">
        <h3 class="page-title">
            Clientes
        </h3>
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb breadcrumb-custom">
                <li class="breadcrumb-item"><a href="">Panel administrador</a></li>
                <li class="breadcrumb-item active" aria-current="page">Clientes</li>
            </ol>
        </nav>
    </div>
    <div class="row">
        <div class="col-lg-12 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">
                    <div class="d-flex justify-content-between">
                        <h4 class="card-title"></h4>
                        <div class="btn-group">
                            <a href="{{route('clients.create')}}" type="button" class="btn btn-info ">
                                <i class="fas fa-plus"></i> Nuevo
                            </a>
                        </div>
                    </div>
                    <div class="table-responsive">
                        <table id="category_listing" class="table tree">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Nombre</th>
                                    
                                    <th>DPI</th>                 
                                    <th>Telefono</th> 
                                    <th>Direccion</th>    
                                    <th>Acciones</th>       
                                </tr>
                            </thead>
                            <tbody>
                                
                                @foreach ($clients as $client)
                                <tr>
                                    <th scope="row">{{$loop->iteration}}</th>
                                    <td>
                                        <a href="{{route('clients.show',$client)}}">{{$client->name}} {{$client->apellido}}</a>
                                    </td> 
                                    <td>{{$client->dpi}}</td>
                                    <td>{{$client->phone}}</td>
                                    <td>{{$client->direccion}}</td>

                                    <td style="width: 75px">
                                        {!! Form::open(['route'=>['clients.destroy',
                                            $client ], 'method'=>'DELETE']) !!}

                                        <a class="jsgrid-button jsgrid-edit-button" href="{{route('clients.edit',$client)}}" title="Editar">
                                            <i class="far fa-edit"></i>
                                        </a>
                                       <button class="jsgrid-button jsgrid-delete-button unstyled-button" type="submit" title="Eliminar">
                                        <i class="far fa-trash-alt"></i>
                                        </button> 
                                       {!! Form::close() !!}
                                    </td>
                                    
                                </tr>

                                @endforeach
                            </tbody>
                        </table>
                        
                    </div>
                </div>
                
             <div class="card-footer text-muted">
                 {{$clients->render()}}
           </div>

            </div>
        </div>
    </div>
</div>
@endsection
@section('scripts')

{!! Html::script('treegrid/js/jquery.treegrid.js') !!}
{!! Html::script('js/my_functions.js') !!}
<script type="text/javascript">
    $(document).ready(function() {
        $('.tree').treegrid().treegrid('collapseAll');
    });
</script>
@endsection
