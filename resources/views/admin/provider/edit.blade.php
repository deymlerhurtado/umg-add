@extends('layouts.admin')

@section('title','Editar Proveedor')
@section('styles')

{!! Html::style('treegrid/css/jquery.treegrid.css') !!}
@endsection
@section('options')
@endsection
@section('preference')
@endsection
@section('content')
<div class="content-wrapper">
    <div class="page-header">
        <h3 class="page-title">
            Editar Proveedor
        </h3>
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb breadcrumb-custom">

                <li class="breadcrumb-item"><a href="">Panel administrador</a></li>
                <li class="breadcrumb-item"><a href="{{route('providers.index')}}">Proveedores</a></li>
                <li class="breadcrumb-item active" aria-current="page">Editar proveedor</li>
            </ol>
        </nav>
    </div>
    <div class="row">
        <div class="col-lg-12 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">
                    <div class="d-flex justify-content-between">
                        <h4 class="card-title">Editar Proveedor</h4>          
                    </div>
                    {!! Form::model($provider,['route'=>['providers.update',$provider],
                        'method'=>'PUT']) !!}
                    
                        <div class="form-group">
                        <label for="names">Nombre Proveedor</label>
                        <input type="text"  class="form-control" name="name" id="name"
                                value="{{$provider->name}}" placeholder="Nombre" aria-describedby="helpId" required>
                        </div>
                        <div class="form-group">
                        <label for="numeros">Numero de Celular</label>
                        <input type="text"  class="form-control" name="numero" id="numero"
                                value="{{$provider->numero}}" placeholder="Numero" aria-describedby="helpId" required>
                         </div>
                         <div class="form-group">
                        <label for="empresas">Nombre Empresa</label>
                        <input type="text"  class="form-control" name="empresa" id="empresa"
                                value="{{$provider->empresa}}" placeholder="Empresa" aria-describedby="helpId" required>
                         </div>
                         <div class="form-group">
                        <label for="emails">Correo Electronico</label>
                        <input type="email"  class="form-control" name="email" id="email" 
                                value="{{$provider->email}}" placeholder="ejemplo@mail.com" aria-describedby="helpId" >
                         </div>


                    <button type="submit" class="btn btn-primary mr-2">Actualizar</button>
                    <a href="{{route('providers.index')}}" class="btn btn-light">Cancelar</a>
                    {!! Form::close() !!}
                </div>
               {{--
                <div class="card-footer text-muted">
                    {{$categories->render()}}
                </div>
                --}}
            </div>
        </div>
    </div>
</div>
@endsection
@section('scripts')

{!! Html::script('treegrid/js/jquery.treegrid.js') !!}
{!! Html::script('js/my_functions.js') !!}
<script type="text/javascript">
    $(document).ready(function() {
        $('.tree').treegrid().treegrid('collapseAll');
    });
</script>
@endsection
