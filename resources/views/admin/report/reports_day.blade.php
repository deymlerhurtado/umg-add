@extends('layouts.admin')

@section('title','Reporte de ventas')
@section('styles')
<style type="text/css">
    .unstyled-button{
        border: none;
        padding: 0;
        background: none; 
    }
</style>
{!! Html::style('treegrid/css/jquery.treegrid.css') !!}
@endsection
@section('options')
@endsection
@section('preference')
@endsection
@section('content')
<div class="content-wrapper">
    <div class="page-header">
        <h3 class="page-title">
            Reporte de Ventas
        </h3>
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb breadcrumb-custom">
                <li class="breadcrumb-item"><a href="">Panel administrador</a></li>
                <li class="breadcrumb-item active" aria-current="page">Reporte de Ventas</li>
            </ol>
        </nav>
    </div>
    <div class="row">
        <div class="col-lg-12 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">
                    <div class="d-flex justify-content-between">
                              
                    </div>
                    <div class="row ">
                        <div class="col-12 col-md-4 text-center">
                            <span>Fecha de consulta: <b> </b></span>
                            <div class="form-group">
                                <strong>{{\Carbon\Carbon::now()->format('d/m/Y')}}</strong>
                            </div>
                        </div>
                        <div class="col-12 col-md-4 text-center">
                            <span>Cantidad de registros: <b></b></span>
                            <div class="form-group">
                                <strong>{{$sales->count()}}</strong>
                            </div>
                        </div>
                        <div class="col-12 col-md-4 text-center">
                            <span>Total de ingresos: <b> </b></span> 
                            <div class="form-group">
                                <strong>Q. {{$total}}</strong>
                            </div>
                        </div>
                    </div>
                    <div class="table-responsive">
                        <table id="sale_listing" class="table tree">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Fecha</th>
                                    <th>Total</th>
                                    <th>Estado</th>
                                    <th style="width:150px">Acciones</th>                   
                                </tr>
                            </thead>
                            <tbody>
                                
                                @foreach ($sales as $sale)
                                <tr>
                                    <th scope="row">{{$loop->iteration}}</th>
                                    <td>
                                    {{$sale->sale_date}}
                                    </td> 

                                    <td>{{$sale->total}}</td>
                                    <td>{{$sale->status}}</td>
                                    <!-- <td style="width: 150px"> -->
                                        <!-- {!! Form::open(['route'=>['sales.destroy',
                                            $sale ], 'method'=>'DELETE']) !!} -->

                                        <!-- <a class="jsgrid-button jsgrid-edit-button" href="{{route('sales.edit',$sale)}}" title="Editar">
                                            <i class="far fa-edit"></i>
                                        </a>
                                       <button class="jsgrid-button jsgrid-delete-button unstyled-button" type="submit" title="Eliminar">
                                        <i class="far fa-trash-alt"></i>
                                        </button>  -->

                                        <td style="width: 20%;">
                                        <a href="{{route('sales.pdf',$sale)}}" class="btn btn-outline-danger"
                                        title="Generar PDF"
                                        ><i class="far fa-file-pdf"></i></a>
                                        <!-- <a href="#" class="btn btn-outline-danger"
                                        title="Generar PDF"
                                        ><i class="fas fa-print"></i></a> -->
                                        <a href="{{route('sales.show',$sale)}}" class="btn btn-outline-info"
                                        title="Ver detalles"
                                        ><i class="far fa-eye"></i></a>
                                        </td>
                                       <!-- {!! Form::close() !!} -->
                                    <!-- </td> -->
                                    
                                </tr>

                                @endforeach
                            </tbody>
                        </table>
                        
                    </div>
                </div>
                <div class="card-footer text-muted">
                    {{$sales->render()}}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('scripts')

{!! Html::script('treegrid/js/jquery.treegrid.js') !!}
{!! Html::script('js/my_functions.js') !!}
<script type="text/javascript">
    $(document).ready(function() {
        $('.tree').treegrid().treegrid('collapseAll');
    });
</script>
@endsection
