
<h3>Permisos especiales</h3>
<div class="form-group">
    <label for="">{!! Form::radio('special', 'all-access') !!} Acceso total</label>
    <label for="">{!! Form::radio('special', 'no-access' ) !!} Ningun acceso</label>
</div>

<h3>Listado de permisos</h3>
<div class="form-group">
    <ul class="list-unstyled">
        @foreach($permissions as $perrmission)
        <li>
            <label for="">
                {!! Form::checkbox('permissions[]', $perrmission->id, null) !!}
                {{$perrmission->name}}
                <em>({{$perrmission->description}})</em>
            </label>
        </li>
        @endforeach
    </ul>
