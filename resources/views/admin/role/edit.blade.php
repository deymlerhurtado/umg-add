@extends('layouts.admin')

@section('title','Editar Rol')
@section('styles')

{!! Html::style('treegrid/css/jquery.treegrid.css') !!}
@endsection
@section('options')
@endsection
@section('preference')
@endsection
@section('content')
<div class="content-wrapper">
    <div class="page-header">
        <h3 class="page-title">
            Editar Rol
        </h3>
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb breadcrumb-custom">

                <li class="breadcrumb-item"><a href="">Panel administrador</a></li>
                <li class="breadcrumb-item"><a href="{{route('roles.index')}}">Roles</a></li>
                <li class="breadcrumb-item active" aria-current="page">Editar Rol</li>
            </ol>
        </nav>
    </div>
    <div class="row">
        <div class="col-lg-12 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">
                    <div class="d-flex justify-content-between">
                        <h4 class="card-title">Editar Rol</h4>          
                    </div>
                    {!! Form::model($role,['route'=>['roles.update',$role],
                        'method'=>'PUT']) !!}
                     <div class="form-group">
                     <label for="names">Nombre</label>
                       <input type="text" name="name" id="name" value="{{$role->name}}" class="form-control"  placeholder="Nombre" required>
                    </div>
                    <div class="form-group">
                    <label for="slugs">Slug</label>
                    <input type="text" name="slug" id="slug"  value="{{$role->slug}}"    class="form-control" placeholder="slug" required>
                    </div>
                    <div class="form-group">
                    <label for="desc">Descripcion</label>
                    <textarea type="description" name="description" id="emadescriptionil" class="form-control" placeholder="descripcion">{{$role->description}}</textarea>
                    </div>
                    @include('admin.role._form')
                    <button type="submit" class="btn btn-primary mr-2">Actualizar</button>
                    <a href="{{route('roles.index')}}" class="btn btn-light">Cancelar</a>
                    {!! Form::close() !!}
                </div>
               {{--
                <div class="card-footer text-muted">
                    {{$roles->render()}}
                </div>
                --}}
            </div>
        </div>
    </div>
</div>
@endsection
@section('scripts')

{!! Html::script('treegrid/js/jquery.treegrid.js') !!}
{!! Html::script('js/my_functions.js') !!}
<script type="text/javascript">
    $(document).ready(function() {
        $('.tree').treegrid().treegrid('collapseAll');
    });
</script>
@endsection
