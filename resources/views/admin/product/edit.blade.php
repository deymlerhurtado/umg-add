@extends('layouts.admin')

@section('title','Editar Producto')
@section('styles')

{!! Html::style('treegrid/css/jquery.treegrid.css') !!}
@endsection
@section('options')
@endsection
@section('preference')
@endsection
@section('content')
<div class="content-wrapper">
    <div class="page-header">
        <h3 class="page-title">
            Editar Productos
        </h3>
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb breadcrumb-custom">

                <li class="breadcrumb-item"><a href="">Panel administrador</a></li>
                <li class="breadcrumb-item"><a href="{{route('products.index')}}">Productos</a></li>
                <li class="breadcrumb-item active" aria-current="page">Edicion de Producto</li>
            </ol>
        </nav>
    </div>
    <div class="row">
        <div class="col-lg-12 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">
                    <div class="d-flex justify-content-between">
                        <h4 class="card-title">Registro de productos</h4>          
                    </div>
                    {!! Form::open(['route'=>['products.update',$product],'method'=>'PUT','files' => true]) !!}
                    <div class="form-group">
                        <label for="names">Nombre Producto</label>
                        <input type="text"  class="form-control" name="name" id="name" value="{{$product->name}}"
                                placeholder="Nombre" aria-describedby="helpId" required>
                    </div>
                    <div class="form-group">
                        <label for="prices">Precio </label>
                        <input type="number" class="form-control" name="price" id="price" value="{{$product->price}}"
                                placeholder="precio" aria-describedby="helpId" required>
                    </div>
                    <div class="form-group">
                        <label for="category_ids">Categoria</label>
                        <select class="form-control" name="category_id" id="category_id">
                        <option value="">Seleccione Categoria</option>
                            @foreach($categories as $category)
                                <option value="{{$category->id}}"
                                @if($category->id ==$product->category_id)
                                selected
                                @endif
                                > {{$category->name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="provider_ids">Proveedor</label>
                        <select class="form-control" name="provider_id" id="provider_id">
                        <option value="">Seleccione Proveedor</option>
                            @foreach($providers as $provider)
                                <option value="{{$provider->id}}" 
                                @if($provider->id==$product->provider_id)
                                selected
                                @endif
                                >{{$provider->name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <!-- <div class="custom-file mb-4">
                        <input type="file" name="image" id="image" class="custom-file-input" lang="es">
                        <label for="" class="custom-file-label" for="images">Cargar Imagen de Producto</label>
                    </div> -->

                    <div class="card-body">
                        <h4 class="card-title d-flex">Imagen de producto
                            <small class="ml-auto align-self-end">
                                <a href="#" class="font-weight-light" 
                                target="_blank"></a>
                            </small>
                        </h4>
                        <input type="file" name="picture" id="picture" class="dropify">
                    </div>

                    <button type="submit" class="btn btn-primary mr-2">Actualizar</button>
                    <a href="{{route('products.index')}}" class="btn btn-light">Cancelar</a>
                    {!! Form::close() !!}
                </div>
               {{--
                <div class="card-footer text-muted">
                    {{$products->render()}}
                </div>
                --}}
            </div>
        </div>
    </div>
</div>
@endsection
@section('scripts')
{{!! Html::script('melody/js/dropify.js') !!}}
{!! Html::script('treegrid/js/jquery.treegrid.js') !!}
{!! Html::script('js/my_functions.js') !!}
<script type="text/javascript">
    $(document).ready(function() {
        $('.tree').treegrid().treegrid('collapseAll');
    });
</script>
@endsection
