<!DOCTYPE>
<html>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="X-UA-Compatible" content="ie=edge">
<title>Reporte de Inventario</title>
<style>
    body {
        /*position: relative;*/
        /*width: 16cm;  */
        /*height: 29.7cm; */
        /*margin: 0 auto; */
        /*color: #555555;*/
        /*background: #FFFFFF; */
        font-family: Arial, sans-serif;
        font-size: 14px;
        /*font-family: SourceSansPro;*/
    }
    #datos {
        float: left;
        margin-top: 0%;
        margin-left: 2%;
        margin-right: 2%;
        /*text-align: justify;*/
    }
    #encabezado {
        text-align: center;
        margin-left: 35%;
        margin-right: 35%;
        font-size: 15px;
    }

    #fact {
        /*position: relative;*/
        float: right;
        margin-top: 2%;
        margin-left: 2%;
        margin-right: 2%;
        font-size: 20px;
        background: ##FFC300;
    }

    section {
        clear: left;
    }

    #cliente {
        text-align: left;
    }

    #faproveedor {
        width: 40%;
        border-collapse: collapse;
        border-spacing: 0;
        margin-bottom: 15px;
    }

    #fac,
    #fv,
    #fa {
        color: #FFFFFF;
        font-size: 15px;
    }

    #faproveedor thead {
        padding: 20px;
        background: #3cb371;
        text-align: left;
        border-bottom: 1px solid #FFFFFF;
    }

    #faccomprador {
        width: 100%;
        border-collapse: collapse;
        border-spacing: 0;
        margin-bottom: 15px;
    }

    #faccomprador thead {
        padding: 20px;
        background: #3cb371;
        text-align: center;
        border-bottom: 1px solid #FFFFFF;
    }

    #facproducto {
        width: 100%;
        border-collapse: collapse;
        border-spacing: 0;
        margin-bottom: 15px;
    }

    #facproducto thead {
        padding: 20px;
        background: #3cb371;
        text-align: center;
        border-bottom: 1px solid #FFFFFF;
    }

</style>

<body>
  
    <header>
        {{--  <div id="logo">
            <img src="{{asset('image/logo-lima.png')}}" alt="" id="imagen">
        </div>  --}}
        <div>
            <table id="datos" border="1">
                <thead>
                    <tr>
                        <th id="">Ferretería la Lima</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <th>
                            Inventario Existencias
                        </th>
                    </tr>
                    <tr>
                        <th>
                            Productos
                        </th>
                    </tr>
                </tbody>
            </table>
        </div>
        <div id="fact">
          {{--  <p>{{$purchase->provider->document_type}} COMPRA<br />
                {{$sale->provider->document_number}}</p>  --}}
                <p>Fecha. {{$date}}</p>
        </div>
    </header>

    <section>
        <br><br>
        <div>
            <table id="facproducto"  border="7">
                <thead>
                    <tr id="fa">
                        <th>No.</th>
                        <th>Categoria</th>
                        <th>Producto</th>
                        <th>Proveedor</th>
                        <th>Stock <br> Unidades</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($products as $product)
                    <tr>
                        <th scope="row" align="center">{{$loop->iteration}}</th>
                        <td align="center">{{$product->category->name}}</td>
                        <td align="center">{{$product->name}}</td>
                        <td align="center">{{$product->provider->name}}</td>
                        @if($product->stock <= 1)
                        <td align="center" style="background-color: red">{{$product->stock}}</td>
                        @else
                        <td align="center">{{$product->stock}}</td>
                        @endif
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </section>
    <br>
    <br>
    <footer>
        <!--puedes poner un mensaje aqui-->
        <div id="datos">
            <p id="encabezado">
                {{--  <b>{{$company->name}}</b><br>{{$company->description}}<br>Telefono:{{$company->telephone}}<br>Email:{{$company->email}}  --}}
            </p>
        </div>
    </footer>
</body>

</html>
