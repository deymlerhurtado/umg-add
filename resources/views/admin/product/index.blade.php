@extends('layouts.admin')

@section('title','Gestión de Productos')
@section('styles')
<style type="text/css">
    .unstyled-button{
        border: none;
        padding: 0;
        background: none; 
    }
</style>
{!! Html::style('treegrid/css/jquery.treegrid.css') !!}
@endsection
@section('options')
@endsection
@section('preference')
@endsection
@section('content')
<div class="content-wrapper">
    <div class="page-header">
        <h3 class="page-title">
            Productos
        </h3>
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb breadcrumb-custom">
                <li class="breadcrumb-item"><a href="">Panel administrador</a></li>
                <li class="breadcrumb-item active" aria-current="page">Productos</li>
            </ol>
        </nav>
    </div>
    <div class="row">
        <div class="col-lg-12 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">
                    <div class="d-flex justify-content-between">
                        <h4 class="card-title"></h4>
                        <div class="btn-group">
                            <a href="{{route('products.create')}}" type="button" class="btn btn-info ">
                                <i class="fas fa-plus"></i> Nuevo
                            </a>
                            <hr><hr><hr><hr>
                            <a href="{{route('products.pdf')}}" class="btn btn-outline-danger" title="Generar PDF" style="width: 20%;">
                                <i class="far fa-file-pdf"></i></a>
                        </div>
                    </div>
                       
                    <div class="table-responsive">
                        <table id="category_listing" class="table tree">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Nombre</th>
                                    <th>Precio</th>
                                    <th>Stock</th>         
                                    <th>Estado</th>        
                                    <th>Categoria</th>    
                                    <th>Proveedor</th>      
                                    <th>Acciones</th>       
                                </tr>
                            </thead>
                            <tbody>
                                
                                @foreach ($products as $product)
                                <tr>
                                    <th scope="row">{{$loop->iteration}}</th>
                                    <td>
                                        <a href="{{route('products.show',$product)}}">{{$product->name}}</a>
                                    </td> 
                                    <td>{{$product->price}}</td>
                                    <td>{{$product->stock}}</td>
                                    @if($product->status == 'ACTIVO')
                                    <td> <a href="{{route('change.status.products',$product)}}" title="Editar" class="jsgrid-button btn btn-success">
                                        Activo <i class="fas fa-check"></i>
                                    </a>
                                    </td>
                                    @else
                                    <td> <a href="{{route('change.status.products',$product)}}" title="Editar" class="jsgrid-button btn btn-danger">
                                        Inactivo <i class="fas fa-times"></i>
                                    </a>
                                    </td>
                                    @endif
                                    <td>{{$product->category->name}}</td>
                                    <td>{{$product->provider->empresa}}</td>
                                    <td style="width: 75px">
                                        {!! Form::open(['route'=>['products.destroy',
                                            $product ], 'method'=>'DELETE']) !!}

                                        <a class="jsgrid-button jsgrid-edit-button" href="{{route('products.edit',$product)}}" title="Editar">
                                            <i class="far fa-edit"></i>
                                        </a>
                                       <button class="jsgrid-button jsgrid-delete-button unstyled-button" type="submit" title="Eliminar">
                                        <i class="far fa-trash-alt"></i>
                                        </button> 
                                       {!! Form::close() !!}
                                    </td>
                                    
                                </tr>

                                @endforeach
                            </tbody>
                        </table>
                        
                    </div>
                </div>
                
             <div class="card-footer text-muted">
                 {{$products->render()}}
           </div>

            </div>
        </div>
    </div>
</div>
@endsection
@section('scripts')

{!! Html::script('treegrid/js/jquery.treegrid.js') !!}
{!! Html::script('js/my_functions.js') !!}
<script type="text/javascript">
    $(document).ready(function() {
        $('.tree').treegrid().treegrid('collapseAll');
    });
</script>
@endsection
