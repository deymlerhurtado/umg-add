<div class="form-group">
     <label for="client_ids">Cliente</label>
    <select class="form-control" name="client_id" id="client_id">
        <option value="">seleccione cliente</option>
        @foreach($clients as $client)
        <option value="{{$client->id}}">{{$client->name}} {{$client->apellido}}</option>
        @endforeach
    </select>
</div>
<!-- <div class="form-group">
    <label for="descriptions">Descripcion de compra</label>
    <input type="text"  class="form-control" name="description" id="description" 
        placeholder="Nombre" aria-describedby="helpId" >
</div> -->
 <div class="form-group">
     <label for="product_ids">Productos</label>
    <select class="form-control" name="product_id" id="product_id">
        <option value="">Seleccione Producto</option>
        @foreach($products as $product)
        <option value="{{$product->id}}_{{$product->stock}}_{{$product->price}}">prd- {{$product->category->name}} {{$product->name}}</option>
        @endforeach
    </select>
</div>
<div class="form-group">
    <label for="">Stock Actual</label>
    <input type="text" name="stock" id="stock" value="" class="form-control" disabled>
</div>
<div class="form-group">
    <label for="quantities">Cantidad Vendida</label>
    <input type="number"  class="form-control" name="quantity" id="quantity" 
        placeholder="cantidad" aria-describedby="helpId" >
</div>
<div class="form-group">
    <label for="prices">Precio</label>
    <input type="number"  class="form-control" name="price" id="price" 
        placeholder="" aria-describedby="helpId" disabled>
</div>
<div class="form-group">
    <label for="discounts">Porcentaje de Descuento</label>
    <input type="number"  class="form-control" name="discount" id="discount" 
        aria-describedby="helpId" value="0">
</div>

<div class="form-group">
    <button type="button" id="agregar" class="btn btn-primary float-right">Agregar Producto</button>
</div>

<div class="form-group">
    <h4 class="card-title">Detalles de venta</h4>
    <div class="table-responsive col-md-12">
         <table id="detalles" class="table table-striped">
            <thead>
                <tr>
                    <th>Eliminar</th>
                    <th>Producto</th>
                    <th>Precio Venta(Q)</th>
                    <th>Descuento%</th>
                    <th>Cantidad vendida</th>
                    <th>ahorro de cliente</th>
                    <th>Sub-total (Q)</th>
                </tr>
            </thead>
            <tfoot>  
                <!-- <tr>
                    <th colspan="6">
                        <p align="right" >TOTAL: </p>
                    </th>
                    <th>
                        <p align="right" ><span id="total">Q. 0.00</span></p>
                    </th>
                </tr> -->
                <tr id="">
                    <th colspan="6">
                        <p align="right" >Total a pagar: </p>
                    </th>
                    <th>
                        <p align="" ><span id="total_pagar_html">Q. 0.00</span>
                            <input type="hidden" name="total" id="total_pagar">
                        </p>
                    </th>
                </tr>
            </tfoot>
        </table>   
    </div>
</div>
