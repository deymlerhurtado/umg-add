<!DOCTYPE>
<html>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="X-UA-Compatible" content="ie=edge">
<title>Reporte de venta</title>
<style>
    body {
        /*position: relative;*/
        /*width: 16cm;  */
        /*height: 29.7cm; */
        /*margin: 0 auto; */
        /*color: #555555;*/
        /*background: #FFFFFF; */
        font-family: Arial, sans-serif;
        font-size: 14px;
        /*font-family: SourceSansPro;*/
    }
    #datos {
        float: left;
        margin-top: 0%;
        margin-left: 2%;
        margin-right: 2%;
        /*text-align: justify;*/
    }
    #encabezado {
        text-align: center;
        margin-left: 35%;
        margin-right: 35%;
        font-size: 15px;
    }
    #fact {
        /*position: relative;*/
        float: right;
        margin-top: 2%;
        margin-left: 2%;
        margin-right: 2%;
        font-size: 15px;
        background: #3cb371;
    }
    section {
        clear: left;
    }
    #cliente {
        text-align: left;
    }
    #faproveedor {
        width: 40%;
        border-collapse: collapse;
        border-spacing: 0;
        margin-bottom: 15px;
    }
    #fac,
    #fv,
    #fa {
        color: #FFFFFF;
        font-size: 15px;
    }
    #faproveedor thead {
        padding: 20px;
        background: #3cb371;
        text-align: left;
        border-bottom: 1px solid #FFFFFF;
    }
    #faccomprador {
        width: 100%;
        border-collapse: collapse;
        border-spacing: 0;
        margin-bottom: 15px;
    }
    #faccomprador thead {
        padding: 20px;
        background: #3cb371;
        text-align: center;
        border-bottom: 1px solid #FFFFFF;
    }

    #facproducto {
        width: 100%;
        border-collapse: collapse;
        border-spacing: 0;
        margin-bottom: 15px;
    }

    #facproducto thead {
        padding: 20px;
        background: #3cb371;
        text-align: center;
        border-bottom: 1px solid #FFFFFF;
    }

</style>

<body>
    <header>
        {{--  <div id="logo">
            <img src="img/logo.png" alt="" id="imagen">
        </div>  --}}
        <div>
            <table id="datos" border="1">
                <thead>
                    <tr>
                        <th id="">VENDEDOR</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <th>
                            <p id="proveedor">Nombre: {{$sale->user->name}}<br>
                                Email: {{$sale->user->email}}</p>
                        </th>
                    </tr>
                </tbody>
            </table>
        </div>
        <div id="fact">
            {{--  <p>{{$purchase->provider->document_type}} COMPRA<br />
                {{$sale->provider->document_number}}</p>  --}}
                <p>Venta No. {{$sale->id}}<br>Fecha: {{$sale->sale_date}}</p>
        </div>
    </header>
    <section>
        <br>
        <br>
        <div>
            <table id="facproducto" border="1">
                <thead>
                    <tr id="fa">
                        <th>No.</th>
                        <th>Producto</th>
                        <th>Cantidad</th>
                        <th>Precio Venta</th>
                        <th>Desc</th>
                        <th>Ahorro</th>
                        <th>Total</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($saleDetails as $saleDetail)
                    <tr>
                        <td scope="row" align="center">{{$loop->iteration}}</td>
                        <td align="center">{{$saleDetail->product->category->name}}<br>{{$saleDetail->product->name}}</td>
                        <td align="center" >{{$saleDetail->quantity}}</td>
                        <td align="center">Q.  {{$saleDetail->price}}</td>
                        <td align="center">{{$saleDetail->discount}}%</td>
                        <td align="center">Q.  {{$saleDetail->quantity*$saleDetail->price*$saleDetail->discount/100,2}}</td>
                        <td align="center">Q.  {{number_format($saleDetail->quantity*$saleDetail->price - $saleDetail->quantity*$saleDetail->price*$saleDetail->discount/100,2)}}</td>
                    </tr>
                    @endforeach
                </tbody>
                <tfoot>
                      <tr>
                        <td colspan="6">
                            <p align="right">Ahorro: </p>
                        </td>
                        <td>
                            <p align="center">Q. {{number_format($ahorro,2)}}</p>
                        </td>
                    </tr>
                    <tr>
                        <th colspan="6">
                            <p align="right">Total: </p>
                        </th>
                        <th>
                            <p align="center">Q. {{number_format($sale->total,2)}}<p>             
                        </th>
                    </tr>        
                </tfoot>
            </table>
        </div>
    </section>
    <br>
    <br>
    <footer>
        <!--puedes poner un mensaje aqui-->
        <div id="datos">
            <p id="encabezado">
                {{--  <b>{{$company->name}}</b><br>{{$company->description}}<br>Telefono:{{$company->telephone}}<br>Email:{{$company->email}}  --}}
            </p>
        </div>
    </footer>
</body>
</html>
