<div class="form-group">
     <label for="provider_ids">Proveedor</label>
    <select class="form-control" name="provider_id" id="provider_id">
        <option value="">Proveedor</option>
        @foreach($providers as $provider)
        <option value="{{$provider->id}}">{{$provider->name}} ({{$provider->empresa}})</option>
        @endforeach
    </select>
</div>
<div class="form-group">
    <label for="descriptions">Descripcion de compra</label>
    <input type="text"  class="form-control" name="description" id="description" 
        placeholder="Nombre" aria-describedby="helpId" >
</div>
 <div class="form-group">
     <label for="product_ids">Productos</label>
    <select class="form-control" name="product_id" id="product_id">
        <option value="">Seleccione Produto</option>
        @foreach($products as $product)
        <option value="{{$product->id}}">prd- {{$product->category->name}} {{$product->name}}</option>
        @endforeach
    </select>
</div>
<!-- input de Search -->
<!-- <div class="form-group">
     <label for="product_search">Buscar Productos</label>
     <input type="text"  class="form-control" name="search" id="search" 
        placeholder="escriba el nombre" aria-describedby="helpId" > <br><br><br><br>
</div> -->

<div class="form-group">
    <label for="quantities">Cantidad Comprada</label>
    <input type="number"  class="form-control" name="quantity" id="quantity" 
        placeholder="cantidad" aria-describedby="helpId" >
</div>
<div class="form-group">
    <label for="prices">Precio de Compra</label>
    <input type="number"  class="form-control" name="price" id="price" 
        placeholder="precio" aria-describedby="helpId" >
</div>

<div class="form-group">
    <button type="button" id="agregar" class="btn btn-primary float-right">Agregar Producto</button>
</div>

<div class="form-group">
    <h4 class="card-title">Detalles de Compra</h4>
    <div class="table-responsive col.md-12">
         <table id="detalles" class="table table-striped">
            <thead>
                <tr>
                    <th>Eliminar</th>
                    <th>Producto</th>
                    <th>Precio (Q)</th>
                    <th>Cantidad</th>
                    <th>Sub-total (Q)</th>
                </tr>
            </thead>
            <tfoot>
                <tr>
                    <th colspan="4">
                        <p align="right" >TOTAL: </p>
                    </th>
                    <th>
                        <p align="right" ><span id="total">Q. 0.00</span></p>
                    </th>
                </tr>
                <tr id="dvOcultar">
                <th colspan="4">
                        <p align="right" >Total a pagar: </p>
                    </th>
                    <th>
                        <p align="right" ><span id="total_pagar_html">Q. 0.00</span>
                            <input type="hidden" name="total" id="total_pagar">
                        </p>
                    </th>
                </tr>
            </tfoot>
        </table>   
    </div>
</div>
