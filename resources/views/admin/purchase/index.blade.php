@extends('layouts.admin')

@section('title','Gestión de compras')
@section('styles')
<style type="text/css">
    .unstyled-button{
        border: none;
        padding: 0;
        background: none; 
    }
</style>
{!! Html::style('treegrid/css/jquery.treegrid.css') !!}
@endsection
@section('options')
@endsection
@section('preference')
@endsection
@section('content')
<div class="content-wrapper">
    <div class="page-header">
        <h3 class="page-title">
            Compras
        </h3>
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb breadcrumb-custom">
                <li class="breadcrumb-item"><a href="">Panel administrador</a></li>
                <li class="breadcrumb-item active" aria-current="page">Compras</li>
            </ol>
        </nav>
    </div>
    <div class="row">
        <div class="col-lg-12 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">
                    <div class="d-flex justify-content-between">
                        <h4 class="card-title"></h4>
                        <div class="btn-group">
                            <a href="{{route('purchases.create')}}" type="button" class="btn btn-info ">
                                <i class="fas fa-plus"></i> Registrar
                            </a>
                        </div>
                    </div>
                    <div class="table-responsive">
                        <table id="purchase_listing" class="table tree">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Fecha</th>
                                    <th>Descripcion</th>
                                    <th>Total (Q)</th>
                                    <th>Estado</th>
                                    <th>Acciones</th>                   
                                </tr>
                            </thead>
                            <tbody>
                                
                                @foreach ($purchases as $purchase)
                                <tr>
                                    <th scope="row">{{$loop->iteration}}</th>
                                    <td>
                                    {{$purchase->purchase_date}}
                                    </td> 
                                    <td><a href="{{route('purchases.show',$purchase)}}">
                                        {{$purchase->description}}</a></td>
                                    <td>{{$purchase->total}}</td>
                                    @if($purchase->status == 'VALID')
                                    <td> <a href="{{route('change.status.purchases',$purchase)}}" title="Editar" class="jsgrid-button btn btn-success">
                                        Valido <i class="fas fa-check"></i>
                                    </a>
                                    </td>
                                    @else
                                    <td> <a href="{{route('change.status.purchases',$purchase)}}" title="Editar" class="jsgrid-button btn btn-danger">
                                        Invalido <i class="fas fa-times"></i>
                                    </a>
                                    </td>
                                    @endif
                                    <!-- <td>{{$purchase->status}}</td> -->

                                    <td style="">
                                        <!-- {!! Form::open(['route'=>['purchases.destroy',
                                            $purchase ], 'method'=>'DELETE']) !!} -->

                                        <!-- <a class="jsgrid-button jsgrid-edit-button" href="{{route('purchases.edit',$purchase)}}" title="Editar">
                                            <i class="far fa-edit"></i>
                                        </a>
                                       <button class="jsgrid-button jsgrid-delete-button unstyled-button" type="submit" title="Eliminar">
                                        <i class="far fa-trash-alt"></i>
                                        </button>  -->

                                        <!-- <td style="width: 20%;"> -->
                                        <a href="{{route('purchases.pdf', $purchase)}}" class="btn btn-outline-danger"
                                        title="Generar PDF"
                                        ><i class="far fa-file-pdf"></i></a>
                                        
                                        <!-- <a href="#" class="btn btn-outline-danger"
                                        title="Generar PDF"
                                        ><i class="fas fa-print"></i></a>  -->
                                        <a href="{{route('purchases.show',$purchase)}}" class="btn btn-outline-info"
                                        title="Ver detalles"
                                        ><i class="far fa-eye"></i></a>
                                        <!-- </td> -->
                                       <!-- {!! Form::close() !!} -->
                                    </td>
                                    
                                </tr>

                                @endforeach
                            </tbody>
                        </table>
                        
                    </div>
                </div>
                <div class="card-footer text-muted">
                    {{$purchases->render()}}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('scripts')

{!! Html::script('treegrid/js/jquery.treegrid.js') !!}
{!! Html::script('js/my_functions.js') !!}
<script type="text/javascript">
    $(document).ready(function() {
        $('.tree').treegrid().treegrid('collapseAll');
    });
</script>
@endsection
