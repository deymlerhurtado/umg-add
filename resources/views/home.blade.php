@extends('layouts.admin')

@section('title','Panel Administador')
@section('styles')
<style type="text/css">
    .unstyled-button{
        border: none;
        padding: 0;
        background: none; 
    }
</style>
<link rel="stylesheet" href="{{asset('jquery-ui/jquery-ui.min.css')}}">
{!! Html::style('treegrid/css/jquery.treegrid.css') !!}
@endsection
@section('options')
@endsection
@section('preference')
@endsection
@section('content')
<div class="content-wrapper">
    <div class="page-header">

        <h3 class="page-title">
           Panel Administración
        </h3>
        <!-- <nav aria-label="breadcrumb">
            <ol class="breadcrumb breadcrumb-custom">
                <li class="breadcrumb-item"><a href="">Panel administrador</a></li>
                <li class="breadcrumb-item active" aria-current="page">Categorías</li>
            </ol>
        </nav> -->
    </div>

    <div class="row">
        <div class="col-lg-12 grid-margin stretch-card">
            <div class="card"><h5>Busqueda rapida de producto</h5>
                <div class="form-group">
                         <label for="descriptions">Escriba el nombre:</label>
                        <input type="text"  class="form-control" name="nombre" id="search" 
                        placeholder="Nombre" aria-describedby="helpId" >
                </div>
                <br><br><br><br><br>Listado
            </div>
        </div>
    </div>


    <div class="row">
        <div class="col-lg-12 grid-margin stretch-card">
            <div class="card">
                
                <div class="card-body">
                    <div class="d-flex justify-content-between">
                     
                    </div>

                    @foreach ($totales as $total)

     <div class="row">
         <div class="col-md-6 grid-margin">
             <div class="card">
                 <div class="card-body">
                     <h4 class="card-title mb-0">Compras</h4>
                     <div class="d-flex justify-content-between align-items-center">
                         <div class="d-inline-block pt-3">
                             <div class="d-md-flex">
                                 <h2 class="mb-0">Q. {{$total->totalcompra}}</h2>
                                 <div class="d-flex align-items-center ml-md-2 mt-2 mt-md-0">
                                     <i class="far fa-clock text-muted"></i>
                                     <small class=" ml-1 mb-0">Dia actual</small>
                                 </div>
                             </div>
                             <small class="text-gray">Compras del sistema</small>
                         </div>
                         <div class="d-inline-block">
                             <i class="fas fa-chart-pie text-info icon-lg"></i>                                    
                         </div>
                     </div>
                 </div>
             </div>
         </div>
         <div class="col-md-6 grid-margin">
             <div class="card">
                 <div class="card-body">
                     <h4 class="card-title mb-0">Ventas</h4>
                     <div class="d-flex justify-content-between align-items-center">
                         <div class="d-inline-block pt-3">
                             <div class="d-md-flex">
                                 <h2 class="mb-0">Q. {{$total->totalventa}}</h2>
                                 <div class="d-flex align-items-center ml-md-2 mt-2 mt-md-0">
                                     <i class="far fa-clock text-muted"></i>
                                     <small class="ml-1 mb-0">Dia actual</small>
                                 </div>
                             </div>
                             <small class="text-gray">Ventas del sistema</small>
                         </div>
                         <div class="d-inline-block">
                             <i class="fas fa-shopping-cart text-danger icon-lg"></i>                                    
                         </div>
                     </div>
                 </div>
             </div>
         </div>
     </div>
     @endforeach     
                </div>

                <div class="row">
        <div class="col-12 grid-margin">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">
                        <i class="fas fa-table"></i>
                        Productos más vendidos
                    </h4>
                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th scope="col">ID</th>
                                    <th>Nombre</th>
                                    <th>Código</th>
                                    <th>Stock</th>
                                    <th>Cantidad vendida</th>
                                  
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($productosvendidos as $productosvendido)
                                <tr>
                                    <td>{{$productosvendido->id}}</td>
                                    <td>{{$productosvendido->name}}</td>
                                    <td>{{$productosvendido->code}}</td>
                                    <td><strong>{{$productosvendido->stock}}</strong> Unidades</td>
                                    <td><strong>{{$productosvendido->quantity}}</strong> Unidades</td>
                                  
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

                <div class="card-footer text-muted">
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('scripts')



{!! Html::script('treegrid/js/jquery.treegrid.js') !!}
{!! Html::script('js/my_functions.js') !!}}
{!! Html::script('js/data-table.js') !!}
{!! Html::script('js/chart.js') !!}
<script type="text/javascript">
    $(document).ready(function() {
        $('.tree').treegrid().treegrid('collapseAll');
    });
</script>

<script src="{{asset('jquery-ui/jquery-ui.min.js')}}" ></script>
<script>
  
    $('#search').autocomplete({
        source: function(request, response){
            $.ajax({
                url: "{{route('search.productos')}}",
                dataType: 'json',
                data: {
                    term: request.term
                },
                success: function(data){
                    response(data)
                }

            });
        }
    });

</script>
@endsection
